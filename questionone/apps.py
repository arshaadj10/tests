from django.apps import AppConfig


class QuestiononeConfig(AppConfig):
    name = 'questionone'
