from django.db import models

# Create your models here.
class patients(models.Model):
    patName = models.TextField()
    refNo = models.TextField()
    idNo = models.TextField()
    dob = models.DateField()
    contact = models.TextField()
    facilName = models.TextField()
    facilAdd = models.TextField()

    def __str__(self):
        return self.patName