from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from . models import patients
from . serializers import patientsSerializer

import csv, io
from django.contrib import messages
from django.contrib.auth.decorators import permission_required


# Create your views here.
class patList(APIView):
    def get(self, request):
        patient1 = patients.objects.all()
        serializer=patientsSerializer(patient1, many=True)
        return Response(serializer.data)

    def post(self):
        pass


#CSV file upload
@permission_required('admin.can_add_log_entry')
def pat_upload(request):
    #template = "contact_upload.html"
    prompt = {
        'order': 'Order facility_name,"facility_address","patient_name","patient_reference_number","patient_id_number","date_of_birth","contact_number"'
    }

    if request.method == "GET":
        return render(request, prompt)

    csv_file = request.FILES['file']
    data_set = csv_file.read().decode('UTF-8')
    io_string = io.StringIO(data_set)
    next(io_string)
    for column in csv.reader(io_string, delimiter=',', quotechar="|"):
        _, created = patients.objects.update_or_create(
            facilName=column[0],
            facilAdd=column[1],
            patName=column[2],
            refNo = column[3],
            idNo = column[4],
            dob = column[5],
            contact = column[6]
        )


